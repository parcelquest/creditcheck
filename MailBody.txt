[Name]

[Company]

[Address]

[City], [State] [Zip]


Dear [Name]:


Our records indicate that the credit card we have on file for your ParcelQuest online service will expire in less than 30 days. In order to avoid any disruption of your service, please contact us to update our records with the new information. We will not bill your card until your next scheduled billing date.


To update the expiration date, simply respond to this email with the new expiration date.


UserID:          [UserID]

Card Number:     [9999] (last four digits)

Expiration Date: [ExpMonth]/[ExpYear]



Thank you,


ParcelQuest

Accounts Administrator

888.217.8999