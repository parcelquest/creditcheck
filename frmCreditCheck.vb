Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Friend Class frmCreditCheck
   Inherits System.Windows.Forms.Form
#Region "Windows Form Designer generated code "
   Public Sub New()
      MyBase.New()
      If m_vb6FormDefInstance Is Nothing Then
         If m_InitializingDefInstance Then
            m_vb6FormDefInstance = Me
         Else
            Try
               'For the start-up form, the first instance created is the default instance.
               If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
                  m_vb6FormDefInstance = Me
               End If
            Catch
            End Try
         End If
      End If
      'This call is required by the Windows Form Designer.
      InitializeComponent()
   End Sub
   'Form overrides dispose to clean up the component list.
   Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
      If Disposing Then
         If Not components Is Nothing Then
            components.Dispose()
         End If
      End If
      MyBase.Dispose(Disposing)
   End Sub
   'Required by the Windows Form Designer
   Private components As System.ComponentModel.IContainer
   Public ToolTip1 As System.Windows.Forms.ToolTip
   Public WithEvents cmdExit As System.Windows.Forms.Button
   Public WithEvents txtExpYear As System.Windows.Forms.TextBox
   Public WithEvents txtExpMonth As System.Windows.Forms.TextBox
   Public WithEvents cmdSendmail As System.Windows.Forms.Button
   Public WithEvents cmdGetList As System.Windows.Forms.Button
   Public WithEvents lstUsers As System.Windows.Forms.CheckedListBox
   Public WithEvents txtToday As System.Windows.Forms.TextBox
   Public WithEvents Label3 As System.Windows.Forms.Label
   Public WithEvents Label2 As System.Windows.Forms.Label
   Friend WithEvents lblCount As System.Windows.Forms.Label
   Public WithEvents Label1 As System.Windows.Forms.Label
   'NOTE: The following procedure is required by the Windows Form Designer
   'It can be modified using the Windows Form Designer.
   'Do not modify it using the code editor.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
      Me.components = New System.ComponentModel.Container()
      Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCreditCheck))
      Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
      Me.cmdExit = New System.Windows.Forms.Button()
      Me.txtExpYear = New System.Windows.Forms.TextBox()
      Me.txtExpMonth = New System.Windows.Forms.TextBox()
      Me.cmdSendmail = New System.Windows.Forms.Button()
      Me.cmdGetList = New System.Windows.Forms.Button()
      Me.lstUsers = New System.Windows.Forms.CheckedListBox()
      Me.txtToday = New System.Windows.Forms.TextBox()
      Me.Label3 = New System.Windows.Forms.Label()
      Me.Label2 = New System.Windows.Forms.Label()
      Me.Label1 = New System.Windows.Forms.Label()
      Me.lblCount = New System.Windows.Forms.Label()
      Me.SuspendLayout()
      '
      'cmdExit
      '
      Me.cmdExit.BackColor = System.Drawing.SystemColors.Control
      Me.cmdExit.Cursor = System.Windows.Forms.Cursors.Default
      Me.cmdExit.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.cmdExit.ForeColor = System.Drawing.SystemColors.ControlText
      Me.cmdExit.Location = New System.Drawing.Point(424, 72)
      Me.cmdExit.Name = "cmdExit"
      Me.cmdExit.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.cmdExit.Size = New System.Drawing.Size(81, 25)
      Me.cmdExit.TabIndex = 9
      Me.cmdExit.Text = "E&xit"
      Me.cmdExit.UseVisualStyleBackColor = False
      '
      'txtExpYear
      '
      Me.txtExpYear.AcceptsReturn = True
      Me.txtExpYear.BackColor = System.Drawing.SystemColors.Window
      Me.txtExpYear.Cursor = System.Windows.Forms.Cursors.IBeam
      Me.txtExpYear.Font = New System.Drawing.Font("Arial", 13.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.txtExpYear.ForeColor = System.Drawing.SystemColors.WindowText
      Me.txtExpYear.Location = New System.Drawing.Point(216, 72)
      Me.txtExpYear.MaxLength = 0
      Me.txtExpYear.Name = "txtExpYear"
      Me.txtExpYear.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.txtExpYear.Size = New System.Drawing.Size(65, 34)
      Me.txtExpYear.TabIndex = 7
      Me.txtExpYear.Tag = "expired year"
      Me.txtExpYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
      '
      'txtExpMonth
      '
      Me.txtExpMonth.AcceptsReturn = True
      Me.txtExpMonth.BackColor = System.Drawing.SystemColors.Window
      Me.txtExpMonth.Cursor = System.Windows.Forms.Cursors.IBeam
      Me.txtExpMonth.Font = New System.Drawing.Font("Arial", 13.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.txtExpMonth.ForeColor = System.Drawing.SystemColors.WindowText
      Me.txtExpMonth.Location = New System.Drawing.Point(144, 72)
      Me.txtExpMonth.MaxLength = 0
      Me.txtExpMonth.Name = "txtExpMonth"
      Me.txtExpMonth.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.txtExpMonth.Size = New System.Drawing.Size(41, 34)
      Me.txtExpMonth.TabIndex = 6
      Me.txtExpMonth.Tag = "expired month"
      Me.txtExpMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
      '
      'cmdSendmail
      '
      Me.cmdSendmail.BackColor = System.Drawing.SystemColors.Control
      Me.cmdSendmail.Cursor = System.Windows.Forms.Cursors.Default
      Me.cmdSendmail.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.cmdSendmail.ForeColor = System.Drawing.SystemColors.ControlText
      Me.cmdSendmail.Location = New System.Drawing.Point(424, 24)
      Me.cmdSendmail.Name = "cmdSendmail"
      Me.cmdSendmail.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.cmdSendmail.Size = New System.Drawing.Size(81, 25)
      Me.cmdSendmail.TabIndex = 4
      Me.cmdSendmail.Tag = "Send mail to selected users"
      Me.cmdSendmail.Text = "&Sendmail"
      Me.cmdSendmail.UseVisualStyleBackColor = False
      '
      'cmdGetList
      '
      Me.cmdGetList.BackColor = System.Drawing.SystemColors.Control
      Me.cmdGetList.Cursor = System.Windows.Forms.Cursors.Default
      Me.cmdGetList.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.cmdGetList.ForeColor = System.Drawing.SystemColors.ControlText
      Me.cmdGetList.Location = New System.Drawing.Point(312, 24)
      Me.cmdGetList.Name = "cmdGetList"
      Me.cmdGetList.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.cmdGetList.Size = New System.Drawing.Size(81, 25)
      Me.cmdGetList.TabIndex = 3
      Me.cmdGetList.Tag = "Get list of expired users"
      Me.cmdGetList.Text = "&Get Users"
      Me.cmdGetList.UseVisualStyleBackColor = False
      '
      'lstUsers
      '
      Me.lstUsers.BackColor = System.Drawing.SystemColors.Window
      Me.lstUsers.Cursor = System.Windows.Forms.Cursors.Default
      Me.lstUsers.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.lstUsers.ForeColor = System.Drawing.SystemColors.WindowText
      Me.lstUsers.Location = New System.Drawing.Point(24, 144)
      Me.lstUsers.Name = "lstUsers"
      Me.lstUsers.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.lstUsers.Size = New System.Drawing.Size(481, 304)
      Me.lstUsers.Sorted = True
      Me.lstUsers.TabIndex = 2
      Me.lstUsers.Tag = "Users about to  have credit card expired "
      '
      'txtToday
      '
      Me.txtToday.AcceptsReturn = True
      Me.txtToday.BackColor = System.Drawing.SystemColors.Menu
      Me.txtToday.Cursor = System.Windows.Forms.Cursors.IBeam
      Me.txtToday.Font = New System.Drawing.Font("Arial", 13.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.txtToday.ForeColor = System.Drawing.SystemColors.WindowText
      Me.txtToday.Location = New System.Drawing.Point(144, 23)
      Me.txtToday.MaxLength = 0
      Me.txtToday.Name = "txtToday"
      Me.txtToday.ReadOnly = True
      Me.txtToday.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.txtToday.Size = New System.Drawing.Size(137, 34)
      Me.txtToday.TabIndex = 1
      '
      'Label3
      '
      Me.Label3.BackColor = System.Drawing.SystemColors.Control
      Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
      Me.Label3.Font = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
      Me.Label3.Location = New System.Drawing.Point(192, 72)
      Me.Label3.Name = "Label3"
      Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.Label3.Size = New System.Drawing.Size(17, 33)
      Me.Label3.TabIndex = 8
      Me.Label3.Text = "/"
      Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter
      '
      'Label2
      '
      Me.Label2.BackColor = System.Drawing.SystemColors.Control
      Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
      Me.Label2.Font = New System.Drawing.Font("Arial", 13.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
      Me.Label2.Location = New System.Drawing.Point(16, 80)
      Me.Label2.Name = "Label2"
      Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.Label2.Size = New System.Drawing.Size(121, 25)
      Me.Label2.TabIndex = 5
      Me.Label2.Text = "Expired date:"
      '
      'Label1
      '
      Me.Label1.BackColor = System.Drawing.SystemColors.Control
      Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
      Me.Label1.Font = New System.Drawing.Font("Arial", 13.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
      Me.Label1.Location = New System.Drawing.Point(24, 32)
      Me.Label1.Name = "Label1"
      Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.Label1.Size = New System.Drawing.Size(105, 25)
      Me.Label1.TabIndex = 0
      Me.Label1.Text = "Today date:"
      Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight
      '
      'lblCount
      '
      Me.lblCount.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.lblCount.ForeColor = System.Drawing.Color.Red
      Me.lblCount.Location = New System.Drawing.Point(312, 80)
      Me.lblCount.Name = "lblCount"
      Me.lblCount.RightToLeft = System.Windows.Forms.RightToLeft.Yes
      Me.lblCount.Size = New System.Drawing.Size(81, 26)
      Me.lblCount.TabIndex = 10
      Me.lblCount.Text = "0"
      '
      'frmCreditCheck
      '
      Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
      Me.BackColor = System.Drawing.SystemColors.Control
      Me.ClientSize = New System.Drawing.Size(526, 496)
      Me.Controls.Add(Me.lblCount)
      Me.Controls.Add(Me.cmdExit)
      Me.Controls.Add(Me.txtExpYear)
      Me.Controls.Add(Me.txtExpMonth)
      Me.Controls.Add(Me.cmdSendmail)
      Me.Controls.Add(Me.cmdGetList)
      Me.Controls.Add(Me.lstUsers)
      Me.Controls.Add(Me.txtToday)
      Me.Controls.Add(Me.Label3)
      Me.Controls.Add(Me.Label2)
      Me.Controls.Add(Me.Label1)
      Me.Cursor = System.Windows.Forms.Cursors.Default
      Me.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
      Me.Location = New System.Drawing.Point(4, 23)
      Me.Name = "frmCreditCheck"
      Me.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.Text = "CreditCheck 1.0"
      Me.ResumeLayout(False)

   End Sub
#End Region
#Region "Upgrade Support "
   Private Shared m_vb6FormDefInstance As frmCreditCheck
   Private Shared m_InitializingDefInstance As Boolean
   Public Shared Property DefInstance() As frmCreditCheck
      Get
         If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
            m_InitializingDefInstance = True
            m_vb6FormDefInstance = New frmCreditCheck()
            m_InitializingDefInstance = False
         End If
         DefInstance = m_vb6FormDefInstance
      End Get
      Set(value As frmCreditCheck)
         m_vb6FormDefInstance = Value
      End Set
   End Property
#End Region

   Private Sub cmdExit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdExit.Click
      Me.Close()
   End Sub

   Private Sub cmdGetList_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGetList.Click
      lstUsers.Items.Clear()
      Call getUserList((txtExpMonth.Text), VB.Right(txtExpYear.Text, 2))
   End Sub

   Private Sub cmdSendmail_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdSendmail.Click
      Dim iTmp, iCnt As Integer
      Dim strMail As String

      iCnt = 0
      For iTmp = 0 To lstUsers.Items.Count - 1
         If lstUsers.GetItemChecked(iTmp) Then
            strMail = VB6.GetItemString(lstUsers, iTmp)
            'If (SendOneMail("Account <sony.nguyen@parcelquest.com>", "sony nguyen <sony1910@gmail.com>", "Testing", gMailBody)) Then
            If (mySendMail(strMail)) Then
               iCnt = iCnt + 1
            Else
               Exit For
            End If
         End If
      Next
      MsgBox("Number of emails sent: " & iCnt)

   End Sub

   Private Sub frmCreditCheck_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
      Dim curDate As Date
      Dim curMonth As Short
      Dim curYear As Short
      Dim iRet As Integer

      curDate = Now
      curMonth = Month(curDate)
      curYear = Year(curDate)

      'Load main
      iRet = subMain()

      If iRet <= 0 Then
         Me.Close()
         Exit Sub
      End If

      Me.Text = "CreditCheck " & My.Application.Info.Version.Major & "." & My.Application.Info.Version.Minor & "." & My.Application.Info.Version.Revision

      'Set expired month and year
      If curMonth = 12 Then
         curMonth = 1
         curYear = curYear + 1
      Else
         curMonth = curMonth + 1
      End If
      txtToday.Text = VB6.Format(curDate, "mm/dd/yyyy")
      txtExpMonth.Text = VB6.Format(curMonth, "00")
      txtExpYear.Text = VB6.Format(curYear, "####")
   End Sub

   Private Sub frmCreditCheck_Closed(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Closed
      closeDB()
   End Sub
End Class