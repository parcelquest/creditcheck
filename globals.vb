Option Strict Off
Option Explicit On

Imports System
Imports System.Diagnostics
Imports VB = Microsoft.VisualBasic


Module modGlobals

   Public dbConn As ADODB.Connection
   Public rsPQ As ADODB.Recordset
   Public gOptDataSource As String
   Public gOptDatabase As String
   Public gOptUserName As String
   Public gOptPassword As String
   Public gMailBody As String
   Public gMailSubject As String
   Public gMailFrom As String
   Public gMailName As String
   Public g_asEmail() As String
   Public g_asCCMail() As String
   Public g_asBCCMail() As String
   Public gMailTestAddr As String
   Public g_iEmail, g_iCCMail, g_iBCCMail As Integer
   Public gLogFile As String
   Public g_bLogOnly As Boolean

   Public Const gcEmail As Short = 0
   Public Const gcName As Short = 1
   Public Const gcCompany As Short = 2
   Public Const gcAddress As Short = 3
   Public Const gcCity As Short = 4
   Public Const gcState As Short = 5
   Public Const gcZip As Short = 6
   Public Const gcCard As Short = 7
   Public Const gcExpMonth As Short = 8
   Public Const gcExpYear As Short = 9
   Public Const gcOrderNo As Short = 10
   Public Const gcUserID As Short = 11

   '***************************************************************************
   Sub LogEvent(sAppName As String, sAppEvent As String)
      Dim sSource As String
      Dim sLog As String
      Dim sEvent As String
      Dim sMachine As String

      sSource = sAppName
      sLog = "Application"
      sEvent = sAppEvent
      sMachine = "."

      If Not EventLog.SourceExists(sSource) Then
         EventLog.CreateEventSource(sSource, sLog)
      End If

      Dim ELog As New EventLog(sLog, sMachine, sSource)
      ELog.WriteEntry(sEvent)
      ELog.WriteEntry(sEvent, EventLogEntryType.Warning, 234, CType(3, Short))

   End Sub

   '***************************************************************************
   Private Function InitSettings() As Boolean
      Dim sTmp As String

      InitSettings = True
      gOptDataSource = My.Settings.Server
      gOptDatabase = My.Settings.DatabaseName
      gOptUserName = "pq"
      gOptPassword = "cdd"
      gMailFrom = My.Settings.MailFrom
      gMailName = My.Settings.MailName
      gMailTestAddr = My.Settings.MailTest

      sTmp = My.Settings.MailCC
      If sTmp > " " Then
         g_iCCMail = ParseStr(sTmp, ";", g_asCCMail)
      End If
      sTmp = My.Settings.MailBCC
      If sTmp > " " Then
         g_iBCCMail = ParseStr(sTmp, ";", g_asBCCMail)
      End If

      sTmp = My.Settings.LogDir
      If sTmp = "" Then
         sTmp = VB6.GetPath
      End If
      gLogFile = sTmp & "\" & VB6.GetEXEName() & ".log"
   End Function

   '***************************************************************************
   Public Function subMain() As Integer
      Dim gInitialized As Boolean
      Dim g_bMonthly, g_bBasic As Boolean
      Dim cmdStr As String
      Dim sTemp As String
      subMain = -1

      gInitialized = InitSettings() 'Initialize settings
      If Not gInitialized Then
         MsgBox("Error initializing CreditCheck: " & Err.Description)
         Exit Function
      End If

      LogMsg("CreditCheck " & My.Application.Info.Version.Major & "." & My.Application.Info.Version.Minor & "." & My.Application.Info.Version.Revision)

      'Validation check
      cmdStr = VB.Command()
      g_bMonthly = False
      g_bBasic = False
      g_bLogOnly = False
      sTemp = GetToken(cmdStr, " ")
      Do While sTemp <> ""
         If sTemp = "-M" Then
            g_bMonthly = True
         ElseIf sTemp = "-B" Then
            g_bBasic = True
         ElseIf sTemp = "-L" Then
            g_bLogOnly = True
         End If
         sTemp = GetToken(sEmpty, " ")
      Loop

      Try
         gMailSubject = readTextFile(VB6.GetPath & "\MailSubject.txt")
         gMailBody = readTextFile(VB6.GetPath & "\Mailbody.txt")
      Catch ex As Exception
         LogMsg("***** Error reading mail files: " & gMailBody & " and " & gMailSubject)
         Exit Function
      End Try

      If g_bBasic Or g_bMonthly Then
         Try
            dbConn = New ADODB.Connection
            If My.Settings.SqlConn > " " Then
               dbConn.ConnectionString = My.Settings.SqlConn
            Else
               'dbConn.ConnectionString = "Provider=SQLOLEDB.1;DATABASE=" & gOptDatabase & ";SERVER=" & gOptDataSource & ";UID=" & gOptUserName & ";PWD=" & gOptPassword
               dbConn.ConnectionString = "Provider=SQLOLEDB.1;trusted_connection=yes;DATABASE=" & gOptDatabase & ";SERVER=" & gOptDataSource
            End If
            dbConn.Open()
         Catch ex As Exception
            LogMsg("***** Error open v_CreditCheck: " & ex.Message())
            Exit Function
         End Try

         'Process monthly account
         If g_bMonthly Then
            Call doChkUsers()
         End If

         dbConn.Close()
         dbConn = Nothing
         subMain = 0
      Else
         subMain = 1
      End If

      LogMsg("End CreditCheck ...")
   End Function

   '***************************************************************************
   Public Sub doChkUsers()
      Dim curDate As Date
      Dim curMonth As Integer
      Dim curYear As Integer
      Dim expDate, iToday As Integer
      Dim iNoEmail, iCnt As Integer
      Dim strError As String
      Dim sql As String
      Dim emailBody, emailName, emailAddr As String
      Dim CardNo As String

      curDate = Now
      curMonth = Month(curDate)
      curYear = Year(curDate)

      LogMsg("Checking CC expiration date on monthly and annual users")

      'Set expired month and year
      If curMonth = 12 Then
         curMonth = 1
         curYear = curYear + 1
      Else
         curMonth = curMonth + 1
      End If
      iToday = curYear * 100 + curMonth

      sql = "SELECT * FROM v_CreditCheck ORDER BY CardNumber"
      Try
         rsPQ = New ADODB.Recordset
         rsPQ.CursorType = ADODB.CursorTypeEnum.adOpenKeyset
         rsPQ.LockType = ADODB.LockTypeEnum.adLockOptimistic
         rsPQ.Open(sql, dbConn, , , ADODB.CommandTypeEnum.adCmdText)
      Catch ex As Exception
         strError = "***** Error opening v_CreditCheck: " & ex.Message()
         LogMsg(strError)
         Exit Sub
      End Try

      'Loop through record set and process everyone
      iCnt = 0
      iNoEmail = 0
      CardNo = " "
      Do While Not rsPQ.EOF
         expDate = rsPQ.Fields("ExpYear").Value
         If expDate < 2000 Then
            expDate += 2000
         End If

         'Format date as yyyymm
         expDate = expDate * 100 + rsPQ.Fields("ExpMonth").Value

         'Send email notification if exp date is smaller today
         If expDate <= iToday Then
            'Multiple accts might use the same CC, notify one only
            If CardNo <> rsPQ.Fields("CardNumber").Value Then
               CardNo = rsPQ.Fields("CardNumber").Value
               If rsPQ.Fields("Email").Value > "0" Then
                  emailAddr = rsPQ.Fields("Email").Value
               Else
                  emailAddr = rsPQ.Fields("L_email").Value
               End If

               If emailAddr > " " Then
                  emailName = StrConv(rsPQ.Fields("Name").Value, VbStrConv.ProperCase)
                  'emailName = """" & emailName & """ <" & strTmp & ">"
                  emailBody = gMailBody
                  emailBody = Replace(emailBody, "[Name]", emailName)
                  emailBody = Replace(emailBody, "[Company]", StrConv(rsPQ.Fields("Company").Value, VbStrConv.ProperCase))
                  emailBody = Replace(emailBody, "[Address]", StrConv(rsPQ.Fields("Address").Value, VbStrConv.ProperCase))
                  emailBody = Replace(emailBody, "[City]", StrConv(rsPQ.Fields("City").Value, VbStrConv.ProperCase))
                  emailBody = Replace(emailBody, "[State]", StrConv(rsPQ.Fields("State").Value, VbStrConv.Uppercase))
                  emailBody = Replace(emailBody, "[Zip]", rsPQ.Fields("Zip").Value)

                  emailBody = Replace(emailBody, "[UserID]", rsPQ.Fields("L_Name").Value)
                  emailBody = Replace(emailBody, "[9999]", Right(rsPQ.Fields("CardNumber").Value, 4))
                  emailBody = Replace(emailBody, "[ExpMonth]", rsPQ.Fields("ExpMonth").Value)
                  emailBody = Replace(emailBody, "[ExpYear]", rsPQ.Fields("ExpYear").Value)
                  emailBody = Replace(emailBody, "[OrderNo]", rsPQ.Fields("OrderNo").Value)

                  If g_bLogOnly Then
                     LogMsg("-->Mail to: " & rsPQ.Fields("ExpMonth").Value & "/" & rsPQ.Fields("ExpYear").Value & " " & emailName & " <" & emailAddr & ">   UserID: " & rsPQ.Fields("UserID").Value)
                     LogMsg(emailBody)
                  Else
                     LogMsg((rsPQ.Fields("UserID").Value & ", " & emailAddr & ", " & rsPQ.Fields("EndDate").Value))
                     Call SendMailEx(gMailFrom, emailAddr, emailName, gMailSubject, emailBody)
                  End If

                  iCnt = iCnt + 1
               Else
                  iNoEmail = iNoEmail + 1
                  LogMsg("*** -->" & rsPQ.Fields("UserID").Value & " has no email address")
               End If
            End If
         End If
         rsPQ.MoveNext()
      Loop

      'Close recordset
      If rsPQ.State = ADODB.ObjectStateEnum.adStateOpen Then
         rsPQ.Close()
      End If
      rsPQ = Nothing

      LogMsg("Number of emails sent:            " & iCnt & vbCrLf)
      If iNoEmail > 0 Then
         LogMsg("Number of accounts without email: " & iNoEmail)
      End If
   End Sub

   '***************************************************************************
   Public Sub getUserList(ByRef expMonth As String, ByRef expYear As String)
      Dim sql, strTmp As String
      Dim expDate, iToday, iCnt As Integer
      Dim curMonth As Integer
      Dim curYear As Integer

      Try
         dbConn = New ADODB.Connection
         If My.Settings.SqlConn > " " Then
            dbConn.ConnectionString = My.Settings.SqlConn
         Else
            dbConn.ConnectionString = "Provider=SQLOLEDB.1;DATABASE=" & gOptDatabase & ";SERVER=" & gOptDataSource & ";UID=" & gOptUserName & ";PWD=" & gOptPassword
         End If

         dbConn.Open()
      Catch ex As Exception
         LogMsg("Error connecting to DB: " & ex.Message())
         Exit Sub
      End Try

      'Adjust expiring date
      If expYear < 100 Then
         curYear = 2000 + expYear
      Else
         curYear = expYear
      End If
      If expMonth = 12 Then
         curMonth = 1
         curYear = curYear + 1
      Else
         curMonth = expMonth + 1
      End If
      iToday = curYear * 100 + curMonth

      'Get list of users
      sql = "SELECT * FROM v_CreditCheck"

      Try
         rsPQ = New ADODB.Recordset
         rsPQ.CursorType = ADODB.CursorTypeEnum.adOpenKeyset
         rsPQ.LockType = ADODB.LockTypeEnum.adLockOptimistic
         rsPQ.Open(sql, dbConn, , , ADODB.CommandTypeEnum.adCmdText)
      Catch ex As Exception
         MsgBox("Error opening table: " & ex.Message())
         Exit Sub
      End Try

      'Loop through record set and process everyone
      strTmp = " "
      iCnt = 0
      Do While Not rsPQ.EOF
         expDate = rsPQ.Fields("ExpYear").Value
         If expDate < 2000 Then
            expDate += 2000
         End If
         expDate = expDate * 100 + rsPQ.Fields("ExpMonth").Value

         If expDate < iToday And (strTmp <> rsPQ.Fields("l_Email").Value) Then
            strTmp = rsPQ.Fields("l_Email").Value
            sql = rsPQ.Fields("l_Email").Value & "|" & StrConv(rsPQ.Fields("Name").Value, VbStrConv.ProperCase)
            sql = sql & "|" & StrConv(rsPQ.Fields("Company").Value, VbStrConv.ProperCase)
            sql = sql & "|" & StrConv(rsPQ.Fields("Address").Value, VbStrConv.ProperCase)
            sql = sql & "|" & StrConv(rsPQ.Fields("City").Value, VbStrConv.ProperCase)
            sql = sql & "|" & StrConv(rsPQ.Fields("State").Value, VbStrConv.Uppercase)
            sql = sql & "|" & rsPQ.Fields("Zip").Value
            sql = sql & "|" & Right(rsPQ.Fields("CardNumber").Value, 4)
            sql = sql & "|" & rsPQ.Fields("ExpMonth").Value
            sql = sql & "|" & rsPQ.Fields("ExpYear").Value
            sql = sql & "|" & rsPQ.Fields("OrderNo").Value
            sql = sql & "|" & rsPQ.Fields("UserID").Value
            'sql = sql & "|" & rsPQ.Fields("UserType").Value
            frmCreditCheck.DefInstance.lstUsers.Items.Add(sql)
         End If
         rsPQ.MoveNext()
      Loop

      'Close recordset
      Call closeDB()
      iCnt = frmCreditCheck.DefInstance.lstUsers.Items.Count
      frmCreditCheck.DefInstance.lblCount.Text = iCnt
   End Sub

   '***************************************************************************
   Public Sub closeDB()
      If Not rsPQ Is Nothing Then
         If rsPQ.State = ADODB.ObjectStateEnum.adStateOpen Then rsPQ.Close()
         rsPQ = Nothing
      End If

      If Not dbConn Is Nothing Then
         If dbConn.State = ADODB.ObjectStateEnum.adStateOpen Then dbConn.Close()
         dbConn = Nothing
      End If
   End Sub

   '***************************************************************************
   Public Function readTextFile(ByRef fileName As String) As String
      Dim strBuf As String
      Dim strTmp As String
      Dim fd As Short

      On Error GoTo Error_readTextFile

      fd = FreeFile()
      FileOpen(fd, fileName, OpenMode.Input)
      strBuf = ""

      Do While Not EOF(fd)
         strTmp = LineInput(fd)
         If strTmp = "" Then
            strBuf = strBuf & vbCrLf
         Else
            strBuf = strBuf & strTmp
         End If
      Loop

      FileClose(fd)
      readTextFile = strBuf
      Exit Function

Error_readTextFile:
      LogMsg("Error reading text file: " & fileName)
      readTextFile = ""
   End Function

    '***************************************************************************
   Public Sub LogMsg(ByVal strMsg As String)
      Dim ff As Short

      Try
         ff = FreeFile()

         FileOpen(ff, gLogFile, OpenMode.Append)
         PrintLine(ff, Now & vbTab & strMsg)
         FileClose(ff)
      Catch ex As Exception
         'Ignore error
      End Try
   End Sub

   '***************************************************************************
   Public Function mySendMail(ByVal sMailInfo As String, Optional ByVal sAttachment As String = "") As Boolean
      Dim strTmp, smtpHost, sMailTo, sMailBody As String
      Dim smtpPort, smtpAcct, smtpPwd, smtpSSL As String
      Dim iTmp As Integer
      Dim myArray(0) As String

      mySendMail = False

      ' message subject
      strTmp = My.Settings.Server
      smtpHost = My.Settings.SmtpHost
      smtpAcct = My.Settings.SmtpUserID
      smtpPwd = My.Settings.SmtpPwd
      smtpPort = My.Settings.SmtpPort
      smtpSSL = My.Settings.SecuredSSL

      iTmp = ParseStr(sMailInfo, "|", myArray)
      sMailTo = myArray(gcEmail)

      sMailBody = gMailBody
      sMailBody = Replace(sMailBody, "[Name]", myArray(gcName))
      sMailBody = Replace(sMailBody, "[Company]", myArray(gcCompany))
      sMailBody = Replace(sMailBody, "[Address]", myArray(gcAddress))
      sMailBody = Replace(sMailBody, "[City]", myArray(gcCity))
      sMailBody = Replace(sMailBody, "[State]", myArray(gcState))
      sMailBody = Replace(sMailBody, "[Zip]", myArray(gcZip))
      sMailBody = Replace(sMailBody, "[9999]", myArray(gcCard))
      sMailBody = Replace(sMailBody, "[ExpMonth]", myArray(gcExpMonth))
      sMailBody = Replace(sMailBody, "[ExpYear]", myArray(gcExpYear))
      sMailBody = Replace(sMailBody, "[OrderNo]", myArray(gcOrderNo))
      sMailBody = Replace(sMailBody, "[UserID]", myArray(gcUserID))

      Try
         Dim mTo As New System.Net.Mail.MailAddress(sMailTo, myArray(gcName))
         Dim mFrom As New System.Net.Mail.MailAddress(gMailFrom)

         'Dim objMessage As New System.Net.Mail.MailMessage(gMailFrom, sMailTo, strSubj, sMailBody)
         Dim objMessage As New System.Net.Mail.MailMessage(mFrom, mTo)
         objMessage.Subject = gMailSubject
         objMessage.Body = sMailBody

         For iTmp = 0 To g_iCCMail - 1
            objMessage.CC.Add(g_asCCMail(iTmp))
         Next
         For iTmp = 0 To g_iBCCMail - 1
            objMessage.Bcc.Add(g_asBCCMail(iTmp))
         Next

         If Not String.IsNullOrEmpty(sAttachment) Then
            objMessage.Attachments.Add(New System.Net.Mail.Attachment(sAttachment))
         End If

         Dim client As New System.Net.Mail.SmtpClient(smtpHost, smtpPort)
         client.Credentials = New System.Net.NetworkCredential(smtpAcct, smtpPwd)
         client.EnableSsl = smtpSSL
         client.Send(objMessage)
         LogMsg("Email has been sent to: " & sMailTo)
         mySendMail = True
      Catch ex As Exception
         Dim ex2 As Exception = ex
         strTmp = ""
         While Not (ex2 Is Nothing)
            strTmp += ex2.ToString()
            ex2 = ex2.InnerException
         End While

         LogMsg("***** Error sending email to: " & sMailTo)
         LogMsg("---> Desc: " & strTmp)
      End Try

   End Function

   Public Function SendOneMail(ByVal sMailFrom As String, ByVal sMailTo As String, ByVal sMailSubj As String, ByVal sMailBody As String, Optional ByVal sAttachment As String = "") As Boolean
      Dim strTmp, smtpHost As String
      Dim smtpPort, smtpAcct, smtpPwd, smtpSSL As String

      SendOneMail = False

      ' message subject
      strTmp = My.Settings.Server
      smtpHost = My.Settings.SmtpHost
      smtpAcct = My.Settings.SmtpUserID
      smtpPwd = My.Settings.SmtpPwd
      smtpPort = My.Settings.SmtpPort
      smtpSSL = My.Settings.SecuredSSL

      'Use this for office365
      'smtp.UseDefaultCredentials = False
      'smtp.Credentials = New NetworkCredential("myEmail@mydomain.com", "MyPassword")
      'smtp.Host = "smtp.office365.com"
      'smtp.port = 587
      'smtp.enablessl = True

      'Use this for Gmail
      'smtp.Credentials = New NetworkCredential("myEmail@mydomain.com", "MyPassword")
      'smtp.Host = "smtp.gmail.com"
      'smtp.port = 587
      'smtp.enablessl = True

      Try
         Dim objMessage As New System.Net.Mail.MailMessage(sMailFrom, sMailTo, sMailSubj, sMailBody)
         Dim iTmp As Integer

         For iTmp = 0 To g_iCCMail - 1
            objMessage.CC.Add(g_asCCMail(iTmp))
         Next
         For iTmp = 0 To g_iBCCMail - 1
            objMessage.Bcc.Add(g_asBCCMail(iTmp))
         Next

         If Not String.IsNullOrEmpty(sAttachment) Then
            objMessage.Attachments.Add(New System.Net.Mail.Attachment(sAttachment))
         End If

         Dim client As New System.Net.Mail.SmtpClient(smtpHost, smtpPort)
         client.Credentials = New System.Net.NetworkCredential(smtpAcct, smtpPwd)
         client.EnableSsl = smtpSSL
         client.Send(objMessage)
         LogMsg("Email has been sent to: " & sMailTo)
         SendOneMail = True
      Catch ex As Exception
         Dim ex2 As Exception = ex
         strTmp = ""
         While Not (ex2 Is Nothing)
            strTmp += ex2.ToString()
            ex2 = ex2.InnerException
         End While

         LogMsg("***** Error sending email to: " & sMailTo)
         LogMsg("---> Desc: " & strTmp)
      End Try

   End Function

   Public Function SendMailEx(ByVal sMailFrom As String, ByVal sMailTo As String, ByVal sMailToName As String, ByVal sMailSubj As String, ByVal sMailBody As String, Optional ByVal sAttachment As String = "") As Boolean
      Dim strTmp, strAddr, strName, smtpHost, smtpPort, smtpAcct, smtpPwd, smtpSSL As String
      Dim iTmp As Integer

      SendMailEx = False

      ' message subject
      strTmp = My.Settings.Server
      smtpHost = My.Settings.SmtpHost
      smtpAcct = My.Settings.SmtpUserID
      smtpPwd = My.Settings.SmtpPwd
      smtpPort = My.Settings.SmtpPort
      smtpSSL = My.Settings.SecuredSSL

      'If test email avail, use it
      If gMailTestAddr > " " Then
         strAddr = gMailTestAddr
         strName = "Tester Email"
         'Disable CC & BCC
         g_iCCMail = 0
         g_iBCCMail = 0
      Else
         strAddr = sMailTo
         strName = sMailToName
      End If

      Try
         Dim mTo As New System.Net.Mail.MailAddress(strAddr, strName)
         Dim mFrom As New System.Net.Mail.MailAddress(gMailFrom)
         Dim objMessage As New System.Net.Mail.MailMessage(mFrom, mTo)

         objMessage.Subject = sMailSubj
         objMessage.Body = sMailBody

         For iTmp = 0 To g_iCCMail - 1
            objMessage.CC.Add(g_asCCMail(iTmp))
         Next
         For iTmp = 0 To g_iBCCMail - 1
            objMessage.Bcc.Add(g_asBCCMail(iTmp))
         Next

         If Not String.IsNullOrEmpty(sAttachment) Then
            objMessage.Attachments.Add(New System.Net.Mail.Attachment(sAttachment))
         End If

         Dim client As New System.Net.Mail.SmtpClient(smtpHost, smtpPort)
         client.Credentials = New System.Net.NetworkCredential(smtpAcct, smtpPwd)
         client.EnableSsl = smtpSSL
         client.Send(objMessage)
         LogMsg("Email has been sent to: " & sMailTo)
         SendMailEx = True
      Catch ex As Exception
         Dim ex2 As Exception = ex
         strTmp = ""
         While Not (ex2 Is Nothing)
            strTmp += ex2.ToString()
            ex2 = ex2.InnerException
         End While

         LogMsg("***** Error sending email to: " & sMailTo)
         LogMsg("---> Desc: " & strTmp)
      End Try

   End Function
End Module